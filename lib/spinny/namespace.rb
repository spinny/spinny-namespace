require "spinny/namespace/version"

module Spinny
  module Namespace
    def namespace(name, &block)
      mod = Module.new do
        self.instance_exec(&block)
      end

      obj = self.is_a?(Module) ? self : self.class

      obj.const_set(name, mod)
    end
  end
end

class Module
  include Spinny::Namespace
end

include Spinny::Namespace
