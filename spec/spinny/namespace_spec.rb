require 'spec_helper'

namespace :Foo do
  def works?
    true
  end
end

RSpec.describe Spinny::Namespace do
  it 'works' do
    expect(Foo.works?).to eq(true)
  end
end
